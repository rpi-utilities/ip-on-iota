FROM debian:latest

RUN apt-get update && \
    apt-get install -y python3 python3-pip sshpass && \
    apt-get clean

RUN pip3 install ansible

COPY . /opt/ip-on-iota

WORKDIR /opt/ip-on-iota

ENTRYPOINT [ "ansible-playbook", "ip_on_iota.yml", "--ask-pass", "-i", "inventories/rpi.yml"]
