#!/usr/bin/env node
'use strict';

/**
 * main.js
 *
 * When this script is executed, it will fetch the network interfaces ad their
 * associated IP address, and then send them to an address in the IOTA Tangle.
 * On first execution, this script will also generate a local JSON file,, in
 * which it will store a pseudorandom seed, and the address it will send to.
 *
 * @link   https://gitlab.com/rpi-utilities/ip-on-iota
 */

const Iota = require('@iota/core');
const converter = require('@iota/converter');
const crypto = require('crypto');
const os = require('os');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const APP = 'ip-on-iota';
const DB_FILE_PATH = `${__dirname}/${APP}.db.json`;
const DEFAULT_TAG = 'IPONIOTA';

let DB_INITIALISED = false;

const iota = Iota.composeAPI({
    provider: 'https://nodes.thetangle.org:443'
});

/**
 * dbInit
 *
 * Initialise the lowDB JSON database file.
 * The db file will be stored in the same directory as this module
 *
 * @return any lowDB instance
 */
const dbInit = () => {
    // Initialise local DB
    const adapter = new FileSync(DB_FILE_PATH, {
        defaultValue: {
            seed: generateSeed()
        }
    });

    DB_INITIALISED = true;
    return low(adapter);
};

/**
 * sendInterfaces
 *
 * Send message containing network interfaces and IP addresses to the IOTA Tangle
 *
 * @param  string seed    IOTA seed
 * @param  string address IOTA address to send transaction to
 * @param  string toSend  Message to send
 */
const sendInterfaces = (seed, address, toSend) => {
    iota.getNodeInfo().then(node => {
        if (Math.abs(node['latestMilestoneIndex'] - node['latestSolidSubtangleMilestoneIndex']) > 3) {
            console.log('Node is probably not synced!');
        } else {
            let message = {
                'address': address,
                'value': 0,
                'message': converter.asciiToTrytes(`{"app": "${APP}",  "timestamp": "${(new Date()).toLocaleString()}", "interfaces": ${toSend}}`),
                'tag': process.env.IP_ON_IOTA_TAG ? process.env.IP_ON_IOTA_TAG : DEFAULT_TAG
            };

            iota.prepareTransfers(seed, [message]).then(trytes => {
                iota.sendTrytes(trytes, 3, 14).then(() => {
                    console.log('Bundle sent.');
                }).catch(error => {
                    console.log(error);
                });

            }).catch(error => {
                console.log(error);
            });
        }
    }).catch(error => {
        console.log(`Request error: ${error.message}`);
    });
};

/**
 * generateSeed
 *
 * Generate a pseudorandom seed, consisting of 81 tryte characters only
 *
 * @return string IOTA seed
 */
const generateSeed = () => {
    const length = 81; // The length of the seed and int array.
    const chars = '9ABCDEFGHIJKLMNOPQRSTUVWXYZ'; // The allowed characters in the seed.
    let result = new Array(length); // An empty array to store the seed characters.
    let randomValues = Buffer.alloc(length);

    // Generate random values and store them to array.
    crypto.randomFillSync(randomValues);

    let cursor = 0; // A cursor is introduced to remove modulus bias.
    for (let i = 0; i < randomValues.length; i++) { // Loop through each of the 81 random values.
        cursor += randomValues[i]; // Add them to the cursor.
        result[i] = chars[cursor % chars.length]; // Assign a new character to the seed based on cursor mod 81.
    }
    return result.join(''); // Merge the array into a single string and return it.
};

/**
 * getIpInterfaces
 *
 * Return the network interfaces and their associated IP addresses
 *
 * @return {interface: string; address: any;}[] Network interfaces
 */
const getIpInterfaces = () => {
    const interfaces = os.networkInterfaces();
    const addresses = [];
    for (let ifname in interfaces) {
        for (let iface in interfaces[ifname]) {
            let address = interfaces[ifname][iface];
            if (address.family === 'IPv4' && !address.internal) {
                addresses.push({
                    interface: ifname,
                    address: address.address
                });
            }
        }
    }
    return(addresses);
};

/**
 * main
 *
 * Main function
 *
 * @return {Promise}
 */
const main = async () => {
    let storage = dbInit();

    if (DB_INITIALISED) {
        if (storage.has('seed').value()) {
            const seed = storage.get('seed').value();
            let iotaAddress;

            if (storage.has('address').value()) {
                iotaAddress = storage.get('address').value();
            } else {
                iotaAddress = await iota.getNewAddress(seed);
                storage.set('address', iotaAddress).write();
            }

            const ipInterfaces = getIpInterfaces();

            sendInterfaces(seed, iotaAddress, JSON.stringify(ipInterfaces));
        }
    }
};

main();
